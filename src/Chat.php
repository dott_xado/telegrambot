<?php

namespace dott_xado\TelegramBot;

class Chat {
	protected $chat;
  	protected $db;

	public function __construct(\dott_xado\TelegramApi\Entity\Chat $chat) {
	    $this->chat = $chat;
	    $this->db = Database::getInstance();
	}

	public function save() {
		if (!$this->verifyExists()) {
		  	$sql = 'insert into chats (id, type, title, first_name, last_name, username, all_members_are_administrators, insert_date) values (:id, :type, :title, :first, :last, :username, :admin, :insert_date)';
		  	$array['id'] = $this->chat->id;
		  	$array['type'] = $this->chat->type;
		  	$array['title'] = (isset($this->chat->title)) ? $this->chat->title : null;
		  	$array['first'] = (isset($this->chat->first_name)) ? $this->chat->first_name : null;
		  	$array['last'] = (isset($this->chat->last_name)) ? $this->chat->last_name : null;
		  	$array['username'] = (isset($this->chat->username)) ? $this->chat->username : null;
		  	$array['admin'] = (isset($this->chat->all_members_are_administrators)) ? $this->chat->all_members_are_administrators : null;
		  	$array['insert_date'] = date('Y-m-d H:i:s');

		  	$this->db->execute($sql, $array);
		}
	}

	protected function verifyExists() {
	    $sql = 'select count(*) from chats where id = :id';
	    $array['id'] = $this->chat->id;

	    $query = $this->db->execute($sql, $array);
	    return $this->db->fetchInner($query)[0] > 0;
	}

	public function getChatType() {
		return $this->chat->type;
	}

	public function getChat() {
		return $this->chat;
	}
}