<?php

namespace dott_xado\TelegramBot\Command;
use \dott_xado\TelegramApi\Output\SendMessage;

class Stat extends \dott_xado\TelegramBot\Command\Command {

  	public function executeCommand($payload) {
	  	if ($this->isAdministrator() !== TRUE) {
	      return null;
	    }
	    if (!is_null($payload) && !is_numeric($payload)) {
	    	$response = new SendMessage(array('text' => 'Devi inviarmi il numero di giorni (incluso oggi) da includere nella statistica! Per esempio: /stat 3', 'chat_id' => $this->chat_id));
	    	return $response;
	    }
	    $toObj = new \DateTime();
		//$toObj->setTime(0, 0, 0);
		$to = $toObj->format('Y-m-d H:i:s');
		$fromObj = new \DateTime();
	    if (is_null($payload)) {
			$fromObj->sub(new \DateInterval('P1D'));
	    } else {
			$fromObj->sub(new \DateInterval('P' . $payload . 'D'));
	    }
	    $fromObj->setTime(0, 0, 0);
			$from = $fromObj->format('Y-m-d H:i:s');
	    $stats = $this->getStats($from, $to);
	    $text = 'Ecco i risultati...Considera che nel totale dei messaggi arrivati c\'è il messaggio che hai appena inviato' . "\n";
	    foreach ($stats as $title => $data) {
	    	$text .= html_entity_decode("&#10145;") . ' ' . $title . ': ';
	    	if (count($data) == 0) {
	    		$text .= html_entity_decode('&#128126;') . "\n";
	    	} else {
	    		foreach ($data as $key => $values) {
		    		$text .= "\n" . $values['date'] . ' -> ' . $values['count'] . "\n";
		    	}
	    	}
	    }
	    return new SendMessage(array('text' => $text, 'chat_id' => $this->chat_id));
  	}

  	protected function getStats($from, $to) {
  		$stats['Nuovi utenti'] = $this->getNewUsers($from, $to);
	    $stats['Eccezzioni'] = $this->getExceptions($from, $to);
	    $stats['Messaggi arrivati'] = $this->getUpdates($from, $to);
	    $stats['Errori da telegram'] = $this->getErrors($from, $to);
	    return $stats;
  	}

  	protected function getNewUsers($from, $to) {
  		$sql = 'select count(*) as count, insert_date::timestamp::date as date from chats where insert_date between :from and :to group by date';
  		$array['from'] = $from;
  		$array['to'] = $to;
  		$query = $this->db->execute($sql, $array);
    	$result = $query->fetchAll(\PDO::FETCH_ASSOC);
    	return $result;
  	}

  	protected function getExceptions($from, $to) {
  		$sql = 'select count(*) as count, insert_data::timestamp::date as date from exceptions where insert_data between :from and :to group by date';
  		$array['from'] = $from;
  		$array['to'] = $to;
  		$query = $this->db->execute($sql, $array);
    	$result = $query->fetchAll(\PDO::FETCH_ASSOC);
    	return $result;
  	}

  	protected function getUpdates($from, $to) {
  		$sql = 'select count(*) as count, insert_data::timestamp::date as date from telegram_updates where insert_data between :from and :to group by date';
  		$array['from'] = $from;
  		$array['to'] = $to;
  		$query = $this->db->execute($sql, $array);
    	$result = $query->fetchAll(\PDO::FETCH_ASSOC);
    	return $result;
  	}

  	protected function getErrors($from, $to) {
  		$sql = 'select count(*) as count, insert_data::timestamp::date as date from telegram_responses where insert_data between :from and :to and ok = 0 group by date';
  		$array['from'] = $from;
  		$array['to'] = $to;
  		$query = $this->db->execute($sql, $array);
    	$result = $query->fetchAll(\PDO::FETCH_ASSOC);
    	return $result;
  	}
}