<?php

namespace dott_xado\TelegramBot\Command;

use dott_xado\TelegramApi\Output\SendMessage;

class Sendtoall extends \dott_xado\TelegramBot\Command\Command {

  public function executeCommand($payload) {
    if ($this->isAdministrator() !== TRUE) {
      return null;
    }
    $this->addMessageToAll($payload);
    $response = new SendMessage(array('text' => 'Messaggio inserito', 'chat_id' => $this->chat_id));
    return $response;
  }

  protected function addMessageToAll($message) {
      $sql = 'insert into message_to_all (chat_id, message) values (:id, :message)';
      $array['id'] = $this->chat_id;
      $array['message'] = $message;
      $this->db->execute($sql, $array);
  }

}