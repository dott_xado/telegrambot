<?php

namespace dott_xado\TelegramBot\Command;

use dott_xado\TelegramApi\Output\SendMessage;


class Help extends \dott_xado\TelegramBot\Command\Command {

  public function executeCommand($payload) {
    $output = $this->db->getText('help', $this->language);


    $response['text'] = $output;
    $response['chat_id'] = $this->chat_id;

    $send = new SendMessage($response);

    return $send;
  }

}