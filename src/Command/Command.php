<?php

namespace dott_xado\TelegramBot\Command;

use dott_xado\TelegramBot\Database;

abstract class Command extends \dott_xado\TelegramBot\Conversation {

	protected $message;
	protected $chat_id;
	protected $language;
	protected $db;

	public function __construct(\dott_xado\TelegramApi\Entity\Message $message) {
		$this->message = $message;
		$this->chat_id = $message->getChatId();
		$this->language = __DEFAULT_LANG__;
		if (isset($message->from)) {
			$this->language = (new \dott_xado\Bot\User($message->from))->getLanguageFromDb();
		}
		$this->db = Database::getInstance();
		parent::__construct(array('chat_id' => $this->chat_id, 'language' => $this->language));
	}

	abstract function executeCommand($payload);

	protected function isAdministrator() {
	    $sql = 'select is_administrator from users where id = :id';
	    $array['id'] = $this->chat_id;
	    $query = $this->db->execute($sql, $array);
	    $result = $this->db->fetchInner($query);
	    if (empty($result)) {
	      return false;
	    }
	    return ($result[0] == 1) ? TRUE : FALSE;
	}
}