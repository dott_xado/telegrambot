<?php

namespace dott_xado\TelegramBot;
use dott_xado\TelegramBot\Exception\ExceptionCatcher;
use dott_xado\TelegramApi\Output\SendMessage;
use dott_xado\TelegramApi\Output\EditMessageText;
use dott_xado\TelegramApi\Entity\InlineKeyboardButton;
use dott_xado\TelegramApi\Entity\InlineKeyboardMarkup;
use dott_xado\TelegramApi\Entity\Update;
use dott_xado\TelegramBot\SendBase;

class Bot extends SendBase {

	protected $update;

	protected $chat_id;

	protected $language;

	public function __construct($update) {
		parent::__construct();
		try {
			$this->logId = $this->log($update);
		} catch (\Exception $e) {
			new ExceptionCatcher('Errore nell\'inserimento in telegram_update. ' . $e->getMessage());
			die;
		}
		try {
			$this->update = new Update($update);
		} catch (\Exception $e) {
			new ExceptionCatcher('Errore nella creazione delle entità. ' . $e->getMessage(), $this->logId);
			die;
		}
	}

	protected function log($update) {
		$query = 'insert into telegram_updates (content) values (:content)';
		$array = array('content' => json_encode($update));
		$this->db->execute($query, $array);
		return $this->db->lastInsertId('telegram_updates_seq');
	}

	public function handleUpdate() {
		$updateType = $this->update->getType();
		$this->chat_id = $this->update->$updateType->getChatId();
		if (is_null($this->chat_id)) {
			die;
		}
		$chat = $this->update->$updateType->getChat();
		if (!is_null($chat)) {
			try {
				$chat = new \dott_xado\Bot\Chat($chat);
				$chat->save();
			} catch (\Exception $e) {
				new ExceptionCatcher('Errore nel salvataggio della chat. ' . $e->getMessage(), $this->logId);
				$this->sendResponse($this->getErrorMessage());
				die;
			}
		}
		
	
		$this->language = __DEFAULT_LANG__;

		if (isset($this->update->$updateType->from)) {
			try {
				$user = new \dott_xado\Bot\User($this->update->$updateType->from);
				$user->save();
			} catch (\Exception $e) {
				new ExceptionCatcher('Errore nel salvataggio dell\'utente. ' . $e->getMessage(), $this->logId);
				$this->sendResponse($this->getErrorMessage());
				die;
			}
			try {
				$language = $user->getLanguageFromDb();
				$this->language = $language;
			 } catch (\Exception $e) {
				new ExceptionCatcher('Errore nel recupero della lingua. ' . $e->getMessage(), $this->logId);
				$this->sendResponse($this->getErrorMessage());
				die;
			}
		}
		$forbidden = $this->checkForbiddenChatType($chat);
		if (!$forbidden) {
			try {
				$response = $this->update->$updateType->handle();
			} catch (\Exception $e) {
				new ExceptionCatcher('Errore nell\'eseguire la richiesta. ' . $e->getMessage(), $this->logId);
				$this->sendResponse($this->getErrorMessage());
				die;
			} catch(\Error $err) {
		      new ExceptionCatcher('Errore nell\'eseguire la richiesta. ' . $err->getMessage(), $this->logId);
				$this->sendResponse($this->getErrorMessage());
				die;
		    }
			if (is_null($response)) {
				$response = $this->getErrorMessage(true);
			}
			$this->sendResponse($response);
		}
	}

	protected function checkForbiddenChatType($chat) {
		return TRUE;
	}

	protected function getErrorMessage($isUserFault = false) {
		if ($isUserFault) {
			$errorMessage['text'] = $this->db->getText('error_message', $this->language);
		} else {
			$errorMessage['text'] = $this->db->getText('system_error_message', $this->language);
		}
		
		$errorMessage['chat_id'] = $this->chat_id;
		$errorMessage = new SendMessage($errorMessage);
		return $errorMessage;
	}

}