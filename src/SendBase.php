<?php

namespace dott_xado\TelegramBot;
use dott_xado\TelegramBot\Exception\ExceptionCatcher;

class SendBase {

	protected $db;

	protected $output;

	protected $logId;

	public function __construct() {
		$this->db = Database::getInstance();
		$temp_output = 'dott_xado\TelegramApi\Output\\' . __SEND_OUTPUT_BY__;
		$this->output = new $temp_output();
	}

	public function sendResponse($response) {
		if (is_array($response)) {
			foreach ($response as $key => $value) {
				try {
					$value->validate();
				} catch (\Exception $e) {
					new ExceptionCatcher('Errore nella validazione dell\'oggetto risposta. ' . $e->getMessage(), $this->logId);
					$value = $this->getErrorMessage();
					break;
				}
				$telegramResponse = $this->output->response($value);
				try {
					$this->logResponse($telegramResponse);  
				} catch (\Exception $e) {
					new ExceptionCatcher('Errore nel salvataggio dell\'oggetto risposta. ' . $e->getMessage(), $this->logId);
				} 
				try {
					$this->updateActiveChat($telegramResponse, $value);
				} catch (\Exception $e) {
					new ExceptionCatcher('Errore nel\'aggiornamento dell\'utente ' . $e->getMessage(), $this->logId);
				}       
				sleep(1);
			}
		} else {
			try {
				$response->validate();
			} catch (\Exception $e) {
				new ExceptionCatcher('Errore nella validazione dell\'oggetto risposta. ' . $e->getMessage(), $this->logId);
				$response = $this->getErrorMessage();
			}
			$telegramResponse = $this->output->response($response);
			try {
				$this->logResponse($telegramResponse);
			} catch (\Exception $e) {
				new ExceptionCatcher('Errore nel salvataggio dell\'oggetto risposta. ' . $e->getMessage(), $this->logId);
			}
			try {
				$this->updateActiveChat($telegramResponse, $response);
			} catch (\Exception $e) {
				new ExceptionCatcher('Errore nel\'aggiornamento dell\'utente ' . $e->getMessage(), $this->logId);
			}
		}
	}

	public function logResponse($response) {
		if ($response) {
			$sql = 'insert into telegram_responses (ok, error_code, description, result) values (:ok, :error, :description, :result)';
			$array['ok'] = (int) $response['ok'];
			$array['error'] = (isset($response['error_code'])) ? $response['error_code'] : null;
			$array['description'] = (isset($response['description'])) ? $response['description'] : null;
			$array['result'] = (isset($response['result'])) ? json_encode($response['result']) : null;
			$this->db->execute($sql, $array);	  
		}
	}

	protected function updateActiveChat($telegramResponse, $response) {
		if (!empty($telegramResponse) && isset($telegramResponse['error_code'])
			&& ($telegramResponse['error_code'] == 403 
			|| ($telegramResponse['error_code'] == 400 && $telegramResponse['description'] == 'Bad Request: have no rights to send a message'))
			&& isset($response->chat_id)) {
			$sql = 'update chats set is_active = 0 where id = :id';
		} else {
			$sql = 'update chats set is_active = 1 where id = :id';
		}
		$array['id'] = (int)$response->chat_id;
		$this->db->execute($sql, $array);
	}

}