<?php

namespace dott_xado\TelegramBot;

class User {

  protected $user;
  protected $db;

  public function __construct(\dott_xado\TelegramApi\Entity\User $user) {
    $this->user = $user;
    $this->db = Database::getInstance();
  }

  public function save() {
    if (!$this->verifyExists()) {
      $sql = 'insert into users (id, first_name, last_name, username, language_code, insert_date) values (:id, :first, :last, :username, :lang, :insert_date)';
      $array['id'] = $this->user->id;
      $array['first'] = $this->user->first_name;
      $array['last'] = (isset($this->user->last_name)) ? $this->user->last_name : null;
      $array['username'] = (isset($this->user->username)) ? $this->user->username : null;
      $array['lang'] = ($this->user->getLanguage()) ? $this->user->getLanguage() : __DEFAULT_LANG__;
      $array['insert_date'] = date('Y-m-d H:i:s');

      $this->db->execute($sql, $array);
    } else {
      $this->updateLanguage();
    }
  }

  protected function verifyExists() {
    $sql = 'select count(*) from users where id = :id';
    $array['id'] = $this->user->id;

    $query = $this->db->execute($sql, $array);
    return $this->db->fetchInner($query)[0] > 0;
  }

  protected function updateLanguage() {
    $sql = 'update users set language_code = :lang where id = :id';
    $array['lang'] = ($this->user->getLanguage()) ? $this->user->getLanguage() : __DEFAULT_LANG__;
    $array['id'] = $this->user->id;

    $this->db->execute($sql, $array);
  }

  public function getLanguageFromDb() {
    $sql = 'select language_code from users where id = :id';
    $array['id'] = $this->user->id;

    $query = $this->db->execute($sql, $array);
    return $this->db->fetchInner($query)[0];
  }
}