<?php

namespace dott_xado\TelegramBot\Exception;

use dott_xado\TelegramBot\Database;

class ExceptionCatcher {

	public $message;

	protected $db;

	protected $telegramUpdatesId;

	public function __construct($message, $telegramUpdatesId = null, $saveOnDb = true) {
		$this->db = Database::getInstance();
		$this->message = $message;
		$this->telegramUpdatesId = $telegramUpdatesId;
		if ($saveOnDb) {
			$this->saveOnDb();
		} else {
			if (__ENVIRONMENT__ != 'production') {
		        var_dump($message);
		    }
		}
	}

	private function saveOnDb() {
		$sql = 'insert into exceptions (message, telegram_updates_id) values (:message, :id)';
		$array['message'] = $this->message;
		$array['id'] = $this->telegramUpdatesId;
		$this->db->execute($sql, $array);
	}

}