<?php

namespace dott_xado\TelegramBot\Exception;

class DbException extends \Exception {

	public $message;

	public function __construct($message, \PDO &$pdo = null, $code = 0, \Exception $previous = null ) {
		parent::__construct($message, $code, $previous);
		$this->message = $message;
		if ( $this->pdo instanceof \PDO && $this->pdo->inTransaction()) {
			$this->pdo->rollBack();
		}
		if (__ENVIRONMENT__ == 'test') {
	        var_dump($e);
	    }
	}
}