<?php

namespace dott_xado\TelegramBot;
use dott_xado\TelegramBot\SendBase;
use dott_xado\TelegramApi\Output\SendMessage;


class SendToAll extends SendBase {

	protected $id;

	protected $chat_id;

	protected $message;

	protected $output;

	public function __construct($row) {
		parent::__construct();
		$this->id = $row->id;
		$this->chat_id = $row->chat_id;
		$this->message = $row->message;
		$this->sendResponse($this->prepareMessages());
		$this->updateSentMessage();
		$this->sendResponse($this->notifyRequester());
	}

	protected function prepareMessages() {
		$allUsers = $this->getAllUsers();
	    $counter = 0;
	    foreach ($allUsers as $key => $value) {
		    $response[$counter] = new SendMessage(array('text' => $this->message, 'chat_id' => $value));
		    $response[$counter]->disableNotification(true);
		    $response[$counter]->setParseMode(2);
		    $counter++;
	    }
	    return $response;
	}

	protected function getAllUsers() {
	    $sql = 'select id from chats where is_active = 1';
	    $query = $this->db->query($sql);
	    $result = $this->db->fetchInner($query);
    	return $result;
    }

	protected function updateSentMessage() {
		$sql = 'update message_to_all set sent_data = now() where id = :id';
		$array['id'] = $this->id;
		$this->db->execute($sql, $array);
	}

	protected function notifyRequester() {
		$message['text'] = 'Ho appena finito di inviare il messaggio a tutti!';
		$message['chat_id'] = $this->chat_id;
		$message = new SendMessage($message);
		return $message;
	}

}