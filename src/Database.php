<?php

namespace dott_xado\TelegramBot;
use dott_xado\TelegramBot\Exception\ExceptionCatcher;

final class Database {

	protected $pdo;

	public static function getInstance() {
		static $instance = null;
		if ($instance === null) {
			$instance = new Database();
		}
		return $instance;
	}

	private function __construct() {
		try {
			$host = __DB_HOST__;
			$dbname = __DB_NAME__;
			$this->pdo = new \PDO("pgsql:host=$host;port=5432;dbname=$dbname", __DB_USER__, __DB_PASSWORD__);
			$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}
		catch (\Exception $e) {
			new ExceptionCatcher($e->getMessage(), null, false);
			die;
		} 
	}

	public function query($sql) {
		try {
			$sql = $this->pdo->query($sql);
			return $sql;
		} catch (\Exception $e){
			$this->rollback();
			throw $e;
		} 
	}

	public function execute($sql, $array) {
		try {			
			$s = $this->pdo->prepare($sql);
			foreach ($array as $key=>$value) {
				$s->bindValue($key, $value);
			}
			$s->execute();
			return $s;
		} catch (\Exception $e) {
			$this->rollback();
			throw $e;
		} 
	}

	public function exec($sql) {
		try {			
			$s = $this->pdo->exec($sql);
			return $s;
		} catch (\Exception $e) {
			$this->rollback();
			throw $e;
		} 
	}

	public function pgsqlGetNotify($time) {
		try {
			$s = $this->pdo->pgsqlGetNotify(\PDO::FETCH_ASSOC, $time); 
			return $s;
		} catch (\Exception $e) {
			$this->rollback();
			throw $e;
		} 
	}

	public function fetchInner(\PDOStatement $r) {
		$response = array();
		$result = $r->fetchAll(\PDO::FETCH_ASSOC);
		foreach ($result as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$response[] = $value2;
			}
		}
		return $response;
	}

	public function getText($label, $lang_code) {
		$sql = 'select text from texts where label = :label and language_code = :lang and is_active = true';

		$array['lang'] = $lang_code;
		$array['label'] = $label;
		$query = $this->execute($sql, $array);
		$result = $this->fetchInner($query);
		if (empty ($result)) {
			$array['lang'] = __DEFAULT_LANG__;
			$array['label'] = $label;
			$query = $this->execute($sql, $array);
			$result = $this->fetchInner($query);
		}

		if (empty ($result)) {
			return ' ';
		}

		$string = (string)$result[0];
		$string = $this->convertEmote($string);

		return $string;
	}

	public function convertEmote($string) {
		$pattern = '/(&\S+?;)/';
		$string = preg_replace_callback($pattern, function($matches) {
			return html_entity_decode($matches[0]);
		}, $string);
		return $string;
	}

	protected function rollback() {
		if ( $this->pdo instanceof \PDO && $this->pdo->inTransaction()) {
			$this->pdo->rollBack();
		}
	}

	public function lastInsertId($sequence) {
		return $this->pdo->lastInsertId($sequence);
	}

}