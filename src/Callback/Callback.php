<?php 

namespace dott_xado\TelegramBot\Callback;

use dott_xado\TelegramBot\Database;

abstract class Callback extends \dott_xado\TelegramBot\Conversation {

	protected $callbackQuery;
	protected $chat_id;
	protected $language;
	protected $db;

	public function __construct(\dott_xado\TelegramApi\Entity\CallbackQuery $cb) {
		$this->callbackQuery = $cb;
		$this->chat_id = $cb->getChatId();
		$this->language = (new \dott_xado\Bot\User($cb->from))->getLanguageFromDb();
		$this->db = Database::getInstance();
		parent::__construct(array('chat_id' => $this->chat_id, 'language' => $this->language));
	}

	abstract function executeCallback($payload);

}