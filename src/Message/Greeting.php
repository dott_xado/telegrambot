<?php

namespace dott_xado\Bot\Message;

class Greeting extends \dott_xado\Bot\Message {

	public function __construct($message) {
		parent::__construct($message);
		$this->setRegexValidation('/\d/');
		$this->setStream(1);
		$this->setStep(1);
	}

	public function handle() {
		$validate = $this->validateConversation($this->message->text, 'Saluto non valido');
		if (!empty($validate)) {
			return $validate;
		}
		$response['text'] = 'Sono una classe che saluta!';
		$response['chat_id'] = $this->chat_id;
		return new \dott_xado\TelegramApi\Output\SendMessage($response);
	}
	
}