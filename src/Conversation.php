<?php

namespace dott_xado\TelegramBot;

abstract class Conversation {

	protected $conversationStream;
	protected $conversationStep;
	protected $regexMessageValidation;
	protected $db;
	protected $chat_id;
	protected $language;

	public function __construct(array $data) {
		$this->db = Database::getInstance();
		$this->chat_id = $data['chat_id'];
		$this->language = $data['language'];
	}

	public function getStream() {
		if (isset($this->conversationStream)) {
			return $this->conversationStream;
		}
	}

	public function getStep() {
		if (isset($this->conversationStep)) {
			return $this->conversationStep;
		}
	}

	public function setStream($stream) {
		$this->conversationStream = $stream;
	}

	public function setStep($step) {
		$this->conversationStep = $step;
	}

	public function setRegexValidation($regex) {
		$this->regexMessageValidation = $regex;
	}

	public function getConversationFromDb() {
		$sql = 'select stream, step from conversations where chat_id = :id';
		$array['id'] = $this->chat_id;

		$query = $this->db->execute($sql, $array);
		$result = $query->fetchAll(\PDO::FETCH_ASSOC);
		if (empty($result)) {
			return array('stream' => 0, 'step' => 0);
		}
		return $result[0];
	}

	public function setConversationInDb() {
		$sql = 'select count(*) as conto from conversations where chat_id = :id';
		$array['id'] = $this->chat_id;
		$query = $this->db->execute($sql, $array);
		$result = $query->fetchAll(\PDO::FETCH_ASSOC);
		if ($result[0]['conto'] > 0) {
			$sql = 'update conversations set stream = :stream, step = :step where chat_id = :id';
			$array['stream'] = $this->getStream();
			$array['step'] = $this->getStep();
			$this->db->execute($sql, $array);
		} else {
			$sql = 'insert into conversations (chat_id, stream, step) values (:id, :stream, :step)';
			$array['stream'] = $this->getStream();
			$array['step'] = $this->getStep();
			$this->db->execute($sql, $array);
		}
	}

	public function validateConversation($text, $notValidMessage) {
		if (isset($this->regexMessageValidation)) {
			$validation = preg_match($this->regexMessageValidation, $text) === 1;
		}
		
		if (!$validation) {
			$response['text'] = $notValidMessage;
			$response['chat_id'] = $this->chat_id;
			return new \dott_xado\TelegramApi\Output\SendMessage($response);
		} else {
			$this->setConversationInDb();
		}
	}
}