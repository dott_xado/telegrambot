<?php

namespace dott_xado\TelegramBot;

class Message extends Conversation {

  protected $message;
  protected $chat_id;
  protected $db;
  protected $conversationsList;

  public function __construct(\dott_xado\TelegramApi\Entity\Message $message) {
    $this->message = $message;
    $this->chat_id = $message->getChatId();
    $this->language = (new \dott_xado\Bot\User($message->from))->getLanguageFromDb();
    $this->db = Database::getInstance();
    parent::__construct(array('chat_id' => $this->chat_id, 'language' => $this->language));
  }

  public function handle() {
    // EXAMPLE TO HANDLE A CONVERSATION
    /* $this->conversationsList[0][0] = '\dott_xado\Bot\Message\Greeting';
    return $this->handleConversation();*/
  }

  protected function handleConversation() {
    $conversation = $this->getConversationFromDb();
    if (empty($this->conversationsList)) {
      return null;
    }
    foreach ($this->conversationsList as $stream => $step) {
      if ($stream === $conversation['stream'] && array_key_exists($conversation['step'], $step)) {
        return (new $step[$conversation['step']]($this->message))->handle();
      }
    }
  }

}